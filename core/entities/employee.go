package entities

import "errors"

type Contract string

const (
	HourlyContract  Contract = "HourlySalaryEmployee"
	MonthlyContract Contract = "MonthlySalaryEmployee"
)

type Employee interface {
	Id() int
	Name() string
	RoleId() int
	RoleName() string
	RoleDescription() string
	ContractType() Contract
	HourlySalary() float64
	MonthlySalary() float64
	AnnualSalary() float64
}

type Role struct {
	Id          int
	Name        string
	Description string
}

type Salary struct {
	Hourly  float64
	Monthly float64
}

type employee struct {
	id     int
	name   string
	role   Role
	salary Salary
}

func NewEmployee(id int, name string, role Role, salary Salary, contractType Contract) (Employee, error) {
	switch contractType {
	case HourlyContract:
		return &HourlySalaryEmployee{
			&employee{id, name, role, salary},
		}, nil

	case MonthlyContract:
		return &MonthlySalaryEmployee{
			&employee{id, name, role, salary},
		}, nil
	}

	return nil, errors.New("The contract type does not exists")
}

func (e *employee) Id() int {
	return e.id
}

func (e *employee) Name() string {
	return e.name
}

func (e *employee) RoleId() int {
	return e.role.Id
}

func (e *employee) RoleName() string {
	return e.role.Name
}

func (e *employee) RoleDescription() string {
	return e.role.Description
}

func (e *employee) HourlySalary() float64 {
	return e.salary.Hourly
}

func (e *employee) MonthlySalary() float64 {
	return e.salary.Monthly
}

type HourlySalaryEmployee struct {
	*employee
}

func (h *HourlySalaryEmployee) ContractType() Contract {
	return HourlyContract
}

func (h *HourlySalaryEmployee) AnnualSalary() float64 {
	return 120 * h.salary.Hourly * 12
}

type MonthlySalaryEmployee struct {
	*employee
}

func (m *MonthlySalaryEmployee) ContractType() Contract {
	return MonthlyContract
}

func (m *MonthlySalaryEmployee) AnnualSalary() float64 {
	return m.salary.Monthly * 12
}
