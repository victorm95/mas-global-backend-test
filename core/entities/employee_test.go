package entities_test

import (
	"mg/core/entities"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewEmployee_Hourly(t *testing.T) {
	result, err := entities.NewEmployee(
		0,
		"",
		entities.Role{},
		entities.Salary{},
		entities.Contract("HourlySalaryEmployee"),
	)

	assert.Nil(t, err)
	assert.IsType(t, new(entities.HourlySalaryEmployee), result)
}

func TestNewEmployee_Monthly(t *testing.T) {
	result, err := entities.NewEmployee(
		0,
		"",
		entities.Role{},
		entities.Salary{},
		entities.Contract("MonthlySalaryEmployee"),
	)

	assert.Nil(t, err)
	assert.IsType(t, new(entities.MonthlySalaryEmployee), result)
}

func TestNewEmployee_Error(t *testing.T) {
	result, err := entities.NewEmployee(
		0,
		"",
		entities.Role{},
		entities.Salary{},
		entities.Contract("Some contract type"),
	)

	assert.Nil(t, result)
	assert.EqualError(t, err, "The contract type does not exists")
}

func TestHourlySalaryEmployee_InterfaceImplementation(t *testing.T) {
	employee, _ := entities.NewEmployee(
		100,
		"Alice",
		entities.Role{1, "Admin", "Can do anything"},
		entities.Salary{10_000.0, 50_000.0},
		entities.HourlyContract,
	)

	assert.Equal(t, 100, employee.Id())
	assert.Equal(t, "Alice", employee.Name())
	assert.Equal(t, 1, employee.RoleId())
	assert.Equal(t, "Admin", employee.RoleName())
	assert.Equal(t, "Can do anything", employee.RoleDescription())
	assert.Equal(t, 10_000.0, employee.HourlySalary())
	assert.Equal(t, 50_000.0, employee.MonthlySalary())
	assert.Equal(t, entities.HourlyContract, employee.ContractType())
}

func TestMonthlySalaryEmployee_InterfaceImplementation(t *testing.T) {
	employee, _ := entities.NewEmployee(
		200,
		"Bob",
		entities.Role{2, "User", "Can login"},
		entities.Salary{100, 200},
		entities.MonthlyContract,
	)

	assert.Equal(t, 200, employee.Id())
	assert.Equal(t, "Bob", employee.Name())
	assert.Equal(t, 2, employee.RoleId())
	assert.Equal(t, "User", employee.RoleName())
	assert.Equal(t, "Can login", employee.RoleDescription())
	assert.Equal(t, 100.0, employee.HourlySalary())
	assert.Equal(t, 200.0, employee.MonthlySalary())
	assert.Equal(t, entities.MonthlyContract, employee.ContractType())
}

func TestAnnualCalculation_HourlySalaryEmployee(t *testing.T) {
	employee, _ := entities.NewEmployee(
		100,
		"Alice",
		entities.Role{1, "Admin", "Can do anything"},
		entities.Salary{10_000.0, 50_000.0},
		entities.HourlyContract,
	)

	assert.Equal(t, 14_400_000.0, employee.AnnualSalary())
}

func TestAnnualCalculation_MonthlySalarayEmployee(t *testing.T) {
	employee, _ := entities.NewEmployee(
		200,
		"Bob",
		entities.Role{2, "User", "Can login"},
		entities.Salary{10_000.0, 50_000.0},
		entities.MonthlyContract,
	)

	assert.Equal(t, 600_000.0, employee.AnnualSalary())
}
