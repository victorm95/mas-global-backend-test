package repositories

import "mg/core/entities"

type EmployeesRepository interface {
	FindAllEmployees() ([]entities.Employee, error)
	FindEmployeeById(id int) (entities.Employee, error)
}
