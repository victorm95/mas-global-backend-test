package employees_test

import (
	"errors"
	"github.com/stretchr/testify/assert"
	usecases "mg/core/usecases/employees"
	"testing"
)

func TestGetEmployeeById_UseCaseExecution(t *testing.T) {
	repositoryMock := new(EmployeesRepositoryMock)
	employeeMock := new(EmployeeMock)
	usecase := usecases.NewGetEmployeeByIdUseCase(repositoryMock)

	repositoryMock.On("FindEmployeeById", 1).Return(employeeMock, nil)

	result, err := usecase.Exec(1)

	assert.NoError(t, err)
	assert.Equal(t, employeeMock, result)
}

func TestGetEmployeeById_UseCaseExecutionWithError(t *testing.T) {
	repositoryMock := new(EmployeesRepositoryMock)
	usecase := usecases.NewGetEmployeeByIdUseCase(repositoryMock)

	repositoryMock.On("FindEmployeeById", 10).Return(nil, errors.New("just an error"))

	result, err := usecase.Exec(10)

	assert.EqualError(t, err, "just an error")
	assert.Equal(t, nil, result)
}
