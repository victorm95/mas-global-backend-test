package employees_test

import (
	"github.com/stretchr/testify/mock"
	"mg/core/entities"
)

type EmployeesRepositoryMock struct {
	mock.Mock
}

func (m *EmployeesRepositoryMock) FindAllEmployees() ([]entities.Employee, error) {
	args := m.Called()

	if args.Get(0) == nil {
		return nil, args.Error(1)
	}

	return args.Get(0).([]entities.Employee), args.Error(1)
}

func (m *EmployeesRepositoryMock) FindEmployeeById(id int) (entities.Employee, error) {
	args := m.Called(id)
	
	if args.Get(0) == nil {
		return nil, args.Error(1)
	}

	return args.Get(0).(entities.Employee), args.Error(1)
}

type EmployeeMock struct {
	mock.Mock
}

func (m *EmployeeMock) Id() int {
	args := m.Called()
	return args.Int(0)
}

func (m *EmployeeMock) Name() string {
	args := m.Called()
	return args.String(0)
}

func (m *EmployeeMock) RoleId() int {
	args := m.Called()
	return args.Int(0)
}

func (m *EmployeeMock) RoleName() string {
	args := m.Called()
	return args.String(0)
}

func (m *EmployeeMock) RoleDescription() string {
	args := m.Called()
	return args.String(0)
}

func (m *EmployeeMock) ContractType() entities.Contract {
	args := m.Called()
	return args.Get(0).(entities.Contract)
}

func (m *EmployeeMock) HourlySalary() float64 {
	args := m.Called()
	return args.Get(0).(float64)
}

func (m *EmployeeMock) MonthlySalary() float64 {
	args := m.Called()
	return args.Get(0).(float64)
}

func (m *EmployeeMock) AnnualSalary() float64 {
	args := m.Called()
	return args.Get(0).(float64)
}
