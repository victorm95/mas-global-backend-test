package employees_test

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"mg/core/entities"
	usecases "mg/core/usecases/employees"
	"testing"
)

func TestGetAllEmployees_UseCaseExecution(t *testing.T) {
	repositoryMock := new(EmployeesRepositoryMock)
	usecase := usecases.NewGetAllEmployeesUseCase(repositoryMock)
	data := []entities.Employee{}

	repositoryMock.On("FindAllEmployees").Return(data, nil)

	result, err := usecase.Exec()

	assert.NoError(t, err)
	assert.Equal(t, data, result)
}

func TestGetAllEmployees_UseCaseExecutionWithError(t *testing.T) {
	repositoryMock := new(EmployeesRepositoryMock)
	usecase := usecases.NewGetAllEmployeesUseCase(repositoryMock)
	data := []entities.Employee{}

	repositoryMock.On("FindAllEmployees").Return(data, errors.New("just an error"))

	result, err := usecase.Exec()

	assert.EqualError(t, err, "just an error")
	assert.Equal(t, data, result)
}
