package employees

import (
	"mg/core/entities"
	"mg/core/repositories"
)

type GetEmployeeByIdUseCase struct {
	repository repositories.EmployeesRepository
}

func NewGetEmployeeByIdUseCase(repository repositories.EmployeesRepository) *GetEmployeeByIdUseCase {
	return &GetEmployeeByIdUseCase{repository}
}

func (u *GetEmployeeByIdUseCase) Exec(id int) (entities.Employee, error) {
	return u.repository.FindEmployeeById(id)
}
