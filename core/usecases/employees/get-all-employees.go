package employees

import (
	"mg/core/entities"
	"mg/core/repositories"
)

type GetAllEmployeesUseCase struct {
	repository repositories.EmployeesRepository
}

func NewGetAllEmployeesUseCase(repository repositories.EmployeesRepository) *GetAllEmployeesUseCase {
	return &GetAllEmployeesUseCase{repository}
}

func (u *GetAllEmployeesUseCase) Exec() ([]entities.Employee, error) {
	return u.repository.FindAllEmployees()
}
