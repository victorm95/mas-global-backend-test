package masglobal

import (
	"bytes"
	"encoding/json"
	"errors"
	"github.com/go-resty/resty/v2"
	"mg/core/entities"
)

type employee struct {
	Id              int     `json:"id"`
	Name            string  `json:"name"`
	ContractType    string  `json:"contractTypeName"`
	RoleId          int     `json:"roleId"`
	RoleName        string  `json:"roleName"`
	RoleDescription string  `json:"roleDescription"`
	HourlySalary    float64 `json:"hourlySalary"`
	MonthlySalary   float64 `json:"monthlySalary"`
}

type EmployeesAdapter struct {
	client *resty.Client
}

func NewEmployeesAdapter() *EmployeesAdapter {
	return &EmployeesAdapter{resty.New()}
}

func (a *EmployeesAdapter) FindAllEmployees() ([]entities.Employee, error) {
	var data []employee

	response, err := a.client.R().
		Get("http://masglobaltestapi.azurewebsites.net/api/employees")

	if err != nil {
		return nil, err
	}

	if err := json.NewDecoder(bytes.NewReader(response.Body())).Decode(&data); err != nil {
		return nil, err
	}

	result := make([]entities.Employee, len(data))

	for i, emp := range data {
		emp, err := entities.NewEmployee(
			emp.Id,
			emp.Name,
			entities.Role{
				Id:          emp.RoleId,
				Name:        emp.RoleName,
				Description: emp.RoleDescription,
			},
			entities.Salary{
				Hourly:  emp.HourlySalary,
				Monthly: emp.MonthlySalary,
			},
			entities.Contract(emp.ContractType),
		)

		if err != nil {
			return nil, err
		}

		result[i] = emp
	}

	return result, nil
}

func (a *EmployeesAdapter) FindEmployeeById(id int) (entities.Employee, error) {
	data, err := a.FindAllEmployees()

	if err != nil {
		return nil, err
	}

	for _, emp := range data {
		if emp.Id() == id {
			return emp, nil
		}
	}

	return nil, errors.New("Employee not found")
}
