module mg/data/masglobal

go 1.16

require (
  github.com/go-resty/resty/v2 v2.4.0
  mg/core v0.0.0
)

replace (
  mg/core => ../../core
)
