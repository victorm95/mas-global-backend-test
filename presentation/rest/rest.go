package rest

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"mg/core/usecases/employees"
	"mg/presentation/rest/handlers"
)

type RestAdapterOptions struct {
	GetEmployeeByIdUseCase *employees.GetEmployeeByIdUseCase
	GetAllEmployeesUseCase *employees.GetAllEmployeesUseCase
}

type RestAdapter struct {
	app *fiber.App
}

func NewRestAdapter(opts *RestAdapterOptions) *RestAdapter {
	adapter := &RestAdapter{fiber.New()}
	adapter.middlewares()
	adapter.routes(opts)

	return adapter
}

func (a *RestAdapter) middlewares() {
	a.app.Use(cors.New())
	a.app.Use(logger.New())
}

func (a *RestAdapter) routes(opts *RestAdapterOptions) {
	a.app.Get("/employees", handlers.Employees(opts.GetAllEmployeesUseCase))
	a.app.Get("/employees/:id", handlers.Employee(opts.GetEmployeeByIdUseCase))
}

func (a *RestAdapter) Listen(addr string) error {
	return a.app.Listen(addr)
}
