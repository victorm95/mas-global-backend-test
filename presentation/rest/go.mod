module mg/presentation/rest

go 1.16

require (
  mg/core v0.0.0
  github.com/gofiber/fiber/v2 v2.9.0
)

replace (
  mg/core => ../../core
)
