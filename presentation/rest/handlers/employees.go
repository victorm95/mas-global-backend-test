package handlers

import (
	"mg/core/entities"
	"mg/core/usecases/employees"

	"github.com/gofiber/fiber/v2"
)

type employee struct {
	Id              int     `json:"id"`
	Name            string  `json:"name"`
	ContractType    string  `json:"contractTypeName"`
	RoleId          int     `json:"roleId"`
	RoleName        string  `json:"roleName"`
	RoleDescription string  `json:"roleDescription"`
	HourlySalary    float64 `json:"hourlySalary"`
	MonthlySalary   float64 `json:"monthlySalary"`
	AnnualSalary    float64 `json:"annualSalary"`
}

func Employees(usecase *employees.GetAllEmployeesUseCase) fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		result, err := usecase.Exec()

		if err != nil {
			return fiber.ErrInternalServerError
		}

		data := make([]*employee, len(result))
		for i, emp := range result {
			data[i] = employeeMapper(emp)
		}

		return ctx.JSON(data)
	}
}

func Employee(usecase *employees.GetEmployeeByIdUseCase) fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		id, err := ctx.ParamsInt("id")
		if err != nil {
			return fiber.ErrBadRequest
		}

		result, err := usecase.Exec(id)

		if err != nil {
			if err.Error() == "Employee not found" {
				return fiber.ErrNotFound
			}
			return fiber.ErrInternalServerError
		}

		return ctx.JSON(employeeMapper(result))
	}
}

func employeeMapper(emp entities.Employee) *employee {
	if emp == nil {
		return new(employee)
	}

	return &employee{
		Id:              emp.Id(),
		Name:            emp.Name(),
		ContractType:    string(emp.ContractType()),
		RoleId:          emp.RoleId(),
		RoleName:        emp.RoleName(),
		RoleDescription: emp.RoleDescription(),
		HourlySalary:    emp.HourlySalary(),
		MonthlySalary:   emp.MonthlySalary(),
		AnnualSalary:    emp.AnnualSalary(),
	}
}
