//+build wireinject

package main

import (
	"github.com/google/wire"
	usecases "mg/core/usecases/employees"
	"mg/core/repositories"
	"mg/presentation/rest"
	"mg/data/masglobal"
)

func NewRest() *rest.RestAdapter {
	wire.Build(
		rest.NewRestAdapter,

		wire.Struct(
			new(rest.RestAdapterOptions),
			"*",
		),
		usecases.NewGetEmployeeByIdUseCase,
		usecases.NewGetAllEmployeesUseCase,

		wire.Bind(
			new(repositories.EmployeesRepository),
			new(*masglobal.EmployeesAdapter),
		),
		masglobal.NewEmployeesAdapter,
	)

	return new(rest.RestAdapter)
}
