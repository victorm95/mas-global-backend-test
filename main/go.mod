module mg/main

go 1.16

require (
	github.com/google/wire v0.5.0
	mg/core v0.0.0
	mg/data/masglobal v0.0.0
	mg/presentation/rest v0.0.0
)

replace (
	mg/core => ../core
	mg/data/masglobal => ../data/masglobal
	mg/presentation/rest => ../presentation/rest
)
